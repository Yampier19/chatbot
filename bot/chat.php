<?php
include "Bot.php";
$bot = new Bot;


$questions = [

    //saludo
    "hola" => "Hola que tal!",
    "un saludo" => "como te va",
    "hello" => "un gusto de verte",
    "holi" => "Hola que tal!",
    "oli" => "Hola que tal!",
    "ola" => "Hola que tal!",
    "holis" => "Hola que tal!",
    "olis" => "Hola que tal!",
    "buenas tardes" => "Hola buenas tardes",
    "buen dia" => "Hola Buen Día",
    "buenas noches" => "Hola Buenas Noches",
    "buenos dias" => "Hola Buenos Días",
    "buenos dias educadora" => "Hola Buenos Días",


    //preguntas ociosas
    "sabes el futuro" => "Quisiera saberlo pero es algo que no alcanzo a calcular, lo que si se es que la tecnologia avanzara muy rapido",

    //MENÚ DE XARELTO////////////////////////////
    "xarelto" => "Quieres saber de <b>XARELTO</b>. Elige un número<br>
   <b>1.</b> Formulacion<br>
   <b>2.</b> Descripción<br>
   <b>3.</b> Instrucciones<br>
   <b>4.</b> Precauciones<br>
   <b>5.</b> Información<br>
   <b>727.</b> Hablar con un Asesor",
    //1. Xarelto Formulacion
    "1" => "Formulación <b>XARELTO</b><br>6. Por cuánto tiempo se debe tomar?<br>7. ¿Puedo partir la pastilla y tomar solo la mitad?<br>8. ¿Puedo tomar Xarelto® y también tomar ASA simultáneamente?<br>9. ¿Cuál es el genérico de Xarelto®?<br>10. ¿Qué pasa si se olvidó la toma de una o más tabletas?",
    //1.6. por cuanto tiempo se debe tomar xarelto
    "6" => "<b>XARELTO</b> Elige una letra<br>6. Por cuánto tiempo se debe tomar?<br>El tiempo dependerá de la indicación y de la prescripción del médico tratante <br>
      <b>Elige una opción:</b><br>
      <b>a.</b> Duración del tratamiento prevención de TEV<br>
      <b>b.</b> Tratamiento de la TVP y el EP<br>
       <b>c.</b> Prevención de ACV en FA<br> 
       <b>d.</b> Tratamiento de Síndrome Coronario Agudo<br>
       <b>e.</b> Prevención secundaria de eventos cardiovasculares",
    //1.6.a
    "a" => "<b>XARELTO</b><br><b>a.</b> Duración del tratamiento prevención de TEV:<br>
    Después de la cirugía mayor de cadera, los pacientes deben recibir tratamiento durante 5 semanas.<br>
    Después de la cirugía mayor de rodilla, los pacientes deben recibir tratamiento durante 2 semanas.",
    //1.6.b
    "b" => "<b>XARELTO</b><br><b>b.</b> Tratamiento de la TVP y el EP:<br>
    Forma y frecuencia de administración:<br>
    Durante las 3 semanas iniciales de tratamiento agudo deben tomarse 15 mg de Xarelto® dos veces al
    día.<br>
    Después de las 3 semanas iniciales, el tratamiento con Xarelto® debe continuarse con 20 mg una vez
    al día. Durante el periodo que su médico tratante estime necesario.<br>
    Después de 6-12 meses de tratamiento, el tratamiento con Xarelto debe continuarse con 20 mg una
    vez al día o 10 mg una vez al día de acuerdo a criterio médico<br>Los comprimidos de Xarelto® de 15 mg y 20 mg deben tomarse con alimentos.<br>",
    //1.6.c
    "c" => "<b>XARELTO</b><br><b>c.</b> Prevención de ACV en FA:<br>
    Duración del tratamiento<br>
    Un comprimido de 20 mg de Xarelto® se debe tomar una vez al día, durante el tiempo que su médico
    tratante estime necesario.<br>
    En pacientes con insuficiencia renal moderada (CrC: <50-30 ml/min) un comprimido de 15 mg de
    Xarelto® se debe tomar una vez al día.<br>
    Los pacientes con fibrilación auricular no valvular que se someten a una PCI con colocación de stent
    deben recibir una dosis reducida de Xarelto 15 mg una vez al día (o Xarelto 10 mg una vez al día
    para pacientes con insuficiencia renal moderada [CrCl: ˂50-30 mL/min]) además de un inhibidor de
    P2Y12. Este régimen de tratamiento es recomendado durante un máximo de 12 meses después de
    una PCI con colocación de stent. Después de la finalización de la terapia antiplaquetaria, la dosis de
    rivaroxabán debe ser incrementada a la dosis estándar para pacientes con fibrilación auricular no
    valvular.<br>Los comprimidos de Xarelto® de 15 mg y 20 mg deben tomarse con alimentos y el estómago lleno.",
    //1.6.d
    "d" => "<b>XARELTO</b><br><b>d.</b> Tratamiento de Síndrome Coronario Agudo con biomarcadores séricos
    positivos: (Rivaroxaban 2,5 mg BID más ASA, Clopidogrel o Ticlopidina)
    Rivaroxaban administrado en combinación con ácido acetilsalicílico
    (ASA) solo, o con ASA más clopidogrel o ticlopidina, está indicado en la
    prevención de eventos aterotrombóticos en pacientes adultos tras un síndrome coronario agudo (SCA) con biomarcadores cardiacos
    elevados.",
    //1.6.e
    "e" => "<b>XARELTO</b><br><b>e.</b> Prevención secundaria de eventos cardiovasculares en pacientes con
    Síndromes Coronarios Crónicos (Rivaroxaban 2,5 mg BID más ASA 100
    mg) o Enfermedad Arterial Periférica Crónica.
    Rivaroxaban administrado concomitantemente con ácido acetilsalicílico
    (ASA) está indicado para la disminución del riesgo de eventos
    aterotrombóticos en pacientes adultos con enfermedad arterial
    coronaria (EAC) o con enfermedad arterial periférica (EAP) sintomática
    con alto riesgo de eventos isquémicos (compromiso de dos o más
    lechos vasculares, historia de falla cardiaca o diabetes).",

    "en cuantos dias aparecen los sintomas?" => "El período de incubación es el tiempo que transcurre entre la infección por el virus y la aparición de los síntomas de la enfermedad. La mayoría de las estimaciones respecto al periodo de incubación de la COVID-19 oscilan entre 1 y 14 días, y en general se sitúan en torno a 5-6 días.",

    //1.7. ¿Puedo partir la pastilla y tomar solo la mitad?
    "7" => "<b>XARELTO</b><br>7. ¿Puedo partir la pastilla y tomar solo la mitad?<br> Bayer no lo recomienda, pues para esto existen varias presentaciones comerciales adecuadas según
la indicación prescrita.",

    //1.8 <b>XARELTO</b><br>
    "8" => "8. ¿Puedo tomar <b>XARELTO</b>® y también tomar ASA simultáneamente?<br>Depende de la recomendación del médico tratante y las indicaciones aprobadas por INVIMA.
Revisar esto Esta respuesta debe ser para indicación de TEV, SPAF o cirugía ortopedica
El uso concomitante de aspirina se ha identificado como un factor de riesgo independiente de
hemorragia mayor en los estudios de eficacia.
Consulte a su médico tratante para que de acuerdo a su historia clínica le brinde la mejor
recomendación para su caso particular.",

    //1.9 ¿Cuál es el genérico de Xarelto®?
    "9" => "9. ¿Cuál es el genérico de <b>XARELTO</b>®?<br>
Rivaroxabán",

    //1.10 ¿Qué pasa si se olvidó la toma de una o más tabletas?
    "10" => "<b>XARELTO</b><br>10. <b>¿Qué pasa si se olvidó la toma de una o más tabletas?</b><br>Prevención de Tromboembolismo Venoso: Dosis olvidadas
Si la dosis se omite, el paciente debe tomar Xarelto® inmediatamente y continuar al día siguiente con
la toma una vez al día, como antes<br>
Tratamiento de la TVP y el EP: Dosis olvidadas<br>
Es esencial cumplir la pauta posológica indicada.<br>
Si se olvida una dosis durante la fase de tratamiento de 15 mg dos veces al día, el paciente debe tomar
Xarelto® inmediatamente, para asegurarse que toma 30 mg de Xarelto® al día. En este caso pueden
tomarse dos comprimidos de 15 mg a la vez. El paciente debe continuar con la toma regular de 15 mg
dos veces al día, como se recomienda, al día siguiente.<br>
Si se olvida una dosis durante la fase de tratamiento de 20 mg una vez al día, el paciente debe tomar
Xarelto® inmediatamente, para asegurarse que toma 20 mg al día. El paciente debe continuar con la
toma regular de 20 mg una vez al día, como se recomienda, al día siguiente.<br>
En el manejo de Síndrome Coronario Agudo o Prevención secundaria de Eventos Cardiovasculares
Mayores en Enfermedad Arterial Coronaria Crónica o Enferemedad Arterial Periférica Crónica, el
paciente debe continuar con su esquema de dosificación habitual recomendada en su siguiente toma.
No debe doblar la dosis.<br>
<b>Prevención de ACV en FA:</b> Dosis olvidadas<br>
Si se ha olvidado una dosis, el paciente debe tomar Xarelto inmediatamente y continuar con la toma
una vez al día, como se recomienda para el día siguiente.<br>
No se debe tomar una dosis doble el mismo día para compensar una dosis olvidada.",

    //2.Descripción
    "2" => "<b>XARELTO</b><br>Elige un número<br> <b>11.</b> Qué es Xarelto®?<br>
<b>12.</b> Cuál es la composición de Xarelto?<br>
<b>13.</b> Cuales son las presentaciones y para que esta indicada cada una?<br>
<b>14.</b> ¿Este producto es nuevo, es confiable?<br>
<b>15.</b> ¿Por qué en la EPS me dicen que no tiene la aprobación del INVIMA?<br>
<b>16.</b> ¿Qué diferencia hay entre la presentación de 10, 15 y 20mg?<br>
<b>17.</b> Cuánto vale Xarelto®?<br>
<b>18.</b> Por qué no viene presentación de 30 tabletas?
",

    //2.11
    "11" => "<b>XARELTO</b><br> 11. Qué es Xarelto®?<br>Xarelto® es la marca comercial para el rivaroxabán.",

    //2.12
    "12" => "<b>XARELTO</b><br> 12. Cuál es la composición de Xarelto®?<br>Componente activo: rivaroxabán",

    //2.13
    "13" => "<b>XARELTO</b> Elige una letra<br> 13. Cuales son las presentaciones y para que esta indicada cada una?<br>
f. Indicación(es) para Xarelto® 10 mg<br>
g. Indicaciones para Xarelto® 15 y 20 mg<br>
h. Indicaciones para Xarelto 2.5 mg BID<br>
i. Posología(tiempo en el que se administra un medicamento) y forma de administración
",

    //2.13.f
    "f" => "<b>XARELTO</b><br>f. Indicación(es) para Xarelto® 10 mg:<br>Xarelto está indicado para la prevención del tromboembolismo venoso (TEV) en pacientes sometidos
a una intervención quirúrgica ortopédica mayor de las extremidades inferiores.<br>
Xarelto está indicado para el tratamiento extendido de la enfermedad tromboembolica venosa (TEV)
en pacientes con alto riesgo de recurrencia<br>
Posología y forma de administración:<br>
Dosis habitual recomendada:<br>
La dosis recomendada para la prevención de TEV en cirugía ortopédica mayor es un comprimido de
10 mg una vez al día.<br>
La dosis recomendada para el tratamiento extendido de TEV es un comprimido de 10 mg una vez al
día",

    //2.13.g
    "g" => "<b>XARELTO</b><br>g. Indicaciones para Xarelto® 15 y 20 mg:<br>
Xarelto® está indicado para la prevención de accidente cerebrovascular y embolismo sistémico en
pacientes con fibrilación auricular no valvular.<br>
Xarelto® está indicado para el tratamiento de la trombosis venosa profunda (TVP) y embolismo
pulmonar (EP), y para la prevención de la TVP y el EP recurrentes.<br>
Xarelto esta indicado en pacientes con fibrilación auricular no valvular que se someten a una PCI
con colocación de stent deben recibir una dosis reducida de Xarelto 15 mg una vez al día (o Xarelto 10 mg una vez al día para pacientes con insuficiencia renal moderada [CrCl: ˂50-30 mL/min])
además de un inhibidor de P2Y12. Este régimen de tratamiento es recomendado durante un máximo
de 12 meses después de una PCI con colocación de stent. Después de la finalización de la terapia
antiplaquetaria, la dosis de rivaroxabán debe ser incrementada a la dosis estándar para pacientes
con fibrilación auricular no valvular.",

    //2.13.h
    "h" => "<b>XARELTO</b><br>h. Indicaciones para Xarelto 2.5 mg BID:<br> Xarelto administrado concomitantemente con ácido acetilsalicílico (ASA) está indicado para la
disminución del riesgo de eventos aterotrombóticos en pacientes adultos con enfermedad arterial
coronaria (EAC) o con enfermedad arterial periférica (EAP) sintomática con alto riesgo de eventos
isquémicos (compromiso de dos o más lechos vasculares, historia de falla cardiaca o diabetes)<br>
Xarelto administrado en combinación con ácido acetilsalicílico (ASA), o con ASA más clopidogrel o
ticlopidina, está indicado en la prevención de eventos aterotrombóticos en pacientesadultos tras un
síndrome coronario agudo (SCA) con biomarcadores cardiacos elevados.
",

    //2.13.i
    "i" => "<b>XARELTO</b><br>i. Posología(tiempo en el que se administra un medicamento) y forma de administración:<br> 
ia. Prevención de ACV en FA<br>
ib. Tratamiento de la TVP y el EP<br>
ic. Tratamiento de PAD y CAD
",

    //2.13.ia
    "ia" => "<b>XARELTO</b><br>ia. Prevención de ACV en FA:<br>
La dosis recomendada es de 20 mg una vez al día vía oral.<br>
En pacientes con insuficiencia renal moderada (depuración de creatinina (CrC): <50-30 ml/min) la
dosis recomendada es de 15 mg una vez al día.",


    //2.13.ib
    "ib" => "<b>XARELTO</b><br>ib. Tratamiento de la TVP y el EP:<br>
Dosis habitual recomendada<br>
La dosis recomendada para el tratamiento inicial de la TVP y el EP agudos es de 15 mg de Xarelto®
dos veces al día durante las primeras tres semanas, seguidos de 20 mg de Xarelto® una vez al día
para el tratamiento continuado y la prevención de TVP recurrente y EP. En pacientes con insuficiencia
renal moderada (depuración de creatinina (CrC): <50-30 ml/min) la dosis recomendada es de 15 mg
una vez al día.
",


    //2.13.ic
    "ic" => "<b>XARELTO</b><br>ic. Tratamiento de PAD y CAD: <br>Dosis habitual recomendada<br>
La dosis recomendada para el tratamiento de la enfermedad arterial periférica y enfermedad coronaria
crónica es de 2.5 mg dos veces al dia más ASA. (1 cada 12 Horas)<br>
Los comprimidos de Xarelto® de 15 mg y 20 mg deben tomarse con alimentos y el estómago lleno.",

    //2.14
    "14" => "<b>XARELTO</b><br>14. ¿Este producto es nuevo, es confiable?<br>Xarelto
Rivaroxabán es un producto que lleva mas de 10 años en el mercado y ha sido aprobado por las
principales agencias regulatorias del mundo luego de cumplir un amplio programa de investigación,
que permitió establecer su eficacia y seguridad.
",

    //2.15
    "15" => "<b>XARELTO</b><br> 15. ¿Por qué en la EPS me dicen que no tiene la aprobación del INVIMA?<br>
Xarelto® cuenta con aprobación INVIMA. Para mayor información dirigirse a la página oficial del
INVIMA. <a href='https://www.invima.gov.co' target='_blank'>www.invima.gov.co</a>",

    //2.16
    "16" => "<b>XARELTO</b><br> 16. ¿Qué diferencia hay entre la presentación de 10, 15 y 20mg?<br>
Cada presentación ha sido evaluada para una indicación de uso específica. Consulte a su médico
tratante si tiene alguna duda.
",

    //2.17
    "17" => "<b>XARELTO</b><br> 17. Cuánto vale Xarelto®?<br>Xarelto® tiene regulación de precios de acuerdo a la circular 03 de 2013. La tableta de Xarelto® está
alrededor de $5030.oo",

    //2.18
    "18" => "<b>XARELTO</b><br> 18. Por qué no viene presentación de 30 tabletas?<br>La presentación que se fabrica para todos los países del mundo es de 14 o 28 tabletas.",

    //3
    "3" => "<b>XARELTO</b><br>3. Instrucciones<br>
19. ¿Puedo comer alimentos de color verde?<br>
20. Como funciona Xarelto®?<br>
21. ¿Por qué el INR no se realiza?<br>
22. ¿Si me dió gripa puedo tomar antigripales?<br>
23. ¿Desde cuándo tiene efectividad Xarelto®?
",

    //3.19
    "19" => "<b>XARELTO</b><br>19. ¿Puedo comer alimentos de color verde?<br>
Sí se puede, rivaroxabán no presenta interacciones con alimentos de la dieta.<br>
Además la absorción oral de rivaroxabán es rápida y alcanza concentraciones máximas entre 2 a 4 h
después de tomar un comprimido. Con la dosis de 20 mg, la biodisponibilidad es del 66% en
condiciones de ayuno; sin embargo, al tomarlo con alimentos, aumentó un 39% lo que indica una
absorción casi completa y una biodisponibilidad oral elevada. Por tanto, rivaroxabán 15 mg y 20 mg
debe tomarse con alimentos.",

    //3.20
    "20" => "<b>XARELTO</b><br>20. Como funciona Xarelto®?<br
Xarelto® es un inhibidor selectivo del FXa. Para actuar, no requiere un cofactor (por ejemplo,
antitrombina III). Rivaroxabán inhibe los FXa libres y la actividad de la protrombinasa. Rivaroxabán no
tiene un efecto directo en la agregación de plaquetas, pero indirectamente inhibe la agregación de
plaquetas inducida por la trombina. Al inhibir el FXa, rivaroxabán reduce la generación de trombina.
",

    //3.21
    "21" => "<b>XARELTO</b><br>21. ¿Por qué el INR no se realiza?<br>
No se realiza debido al perfil de anticoagulación predecible de Xarelto®, por lo cual no requiere de
monitoreo de la coagulación de rutina. La prueba del INR no debe realizarse para medir los niveles de anticoagulación con Xarelto® debido a que esta prueba fue diseñada específicamente para medir los
niveles de anticoagulación con los antagonistas de la vitamina K y no puede ser usado para ningún
otro anticoagulante.",

    //3.22
    "22" => "<b>XARELTO</b><br>22. ¿Si me dió gripa puedo tomar antigripales?<br>
La ficha técnica del producto no indica si existen o no interacciones específicas con los antigripales,
por lo cual se recomienda que consulte con su médico tratante.
",

    //3.23
    "23" => "<b>XARELTO</b><br>23. ¿Desde cuándo tiene efectividad Xarelto®?<br>
Las concentraciones máximas (Cmáx) del rivaroxabán aparecen de 2 a 4 horas
después de ingerir la tableta, teniendo un inicio de acción rápido.
",

    //4
    "4" => "<b>XARELTO<br>4. Precauciones:</b><br>
24. ¿Qué puedo hacer si presento una hemorragia o me corto?<br>
25. ¿Si voy de viaje largo que precauciones debo tener?<br>
26. ¿Qué hago en caso de una hemorragia ?<br>
27. ¿Si me van a realizar un procedimiento quirúrgico cuantos días antes debo suspenderlo y cuando debo reiniciarlo, y que riesgo tengo en este tiempo?<br>
28. ¿Si tengo gastritis, la toma de Xarelto® aumenta más los síntomas o me agrava la enfermedad?<br>
29. ¿Cuáles son las Interacciones que pueden presentarse con Xarelto®?<br>
30. ¿Qué contraindicaciones tiene Xarelto®?<br>
31. ¿Cuáles son los Eventos Adversos asociados con Xarelto®?<br>
32. ¿Puedo usar Xarelto® en estado de Embarazo o lactancia?<br>
33. Qué se debe hacer en caso de sobredosis?<br>
34. ¿Cuáles son las Precauciones especiales de conservación y manipulación?<br>
35. ¿Qué medicamentos no puedo consumir si tomo Xarelto®?
",

    //4.24
    "24" => "<b>XARELTO</b><br>24. ¿Qué puedo hacer si presento una hemorragia o me corto?<br>
Lo más importante es que consulte a su médico tratante y no demore en acudir a su servicio de salud
para una pronta atención.<br>
Su médico podrá considerar alguna de las siguientes alternativas:<br>
Si se presentara una complicación hemorrágica en un paciente que recibe rivaroxabán, la próxima
administración debe retrasarse o el tratamiento debe suspenderse, según sea necesario. El
rivaroxabán tiene una vida media de aproximadamente 5 a 13 horas. El tratamiento debe
individualizarse en función de la gravedad y localización de la hemorragia. Puede realizarse
tratamiento sintomático adecuado, según sea necesario, como compresión mecánica (por ejemplo,
para la epistaxis grave), hemostasia quirúrgica con procedimientos de control de la hemorragia,
reemplazo de líquidos y soporte hemodinámico, hemoderivados (concentrado de eritrocitos o plasma
fresco congelado, dependiendo de la anemia o coagulopatía asociada) o plaquetas.<br>
Si la hemorragia no puede controlarse por las medidas anteriores, su médico tratante podría considerar
la administración de un agente reversor procoagulante específico, como concentrado de complejo de
protrombina (PCC), concentrado de complejo de protrombina activada (APCC) o factor VIIa
recombinante (r-FVIIa). Sin embargo, actualmente hay una experiencia clínica muy limitada con el uso
de estos productos en las personas que reciben Xarelto®.<br>
No se espera que el sulfato de protamina y la vitamina K afecten a la actividad anticoagulante del
rivaroxabán. 
",

    //4.25
    "25" => "<b>XARELTO</b><br>25. ¿Si voy de viaje largo que precauciones debo tener?<br>
    Las precauciones que le indique su médico tratante quien es quien mejor conoce su historia clínica.",

    //4.26
    "26" => "<b>XARELTO</b><br>26. ¿Qué hago en caso de una hemorragia ?<br>",

    //4.27
    "27" => "<b>XARELTO</b><br>27. ¿Si me van a realizar un procedimiento quirúrgico cuantos días antes debo suspenderlo y cuando debo reiniciarlo, y que riesgo tengo en este tiempo?<br>
    Si se requiere un procedimiento invasivo o una intervención quirúrgica, Xarelto® debe interrumpirse al menos 24 horas antes de la intervención, si es posible y en base a la valoración clínica del médico.
Si el procedimiento no puede retrasarse, se debe evaluar el aumento del riesgo de hemorragia frente a la urgencia de la intervención.<br>
Xarelto® debe reiniciarse lo antes posible, después del procedimiento invasivo o de la intervención quirúrgica, siempre que lo permita la situación clínica y se haya establecido una hemostasia adecuada la recomendación es reiniciar 6-8 horas después de confirmación de hemostasia. ",

    //4.28
    "28" => "<b>XARELTO</b><br>28. ¿Si tengo gastritis, la toma de Xarelto® aumenta más los síntomas o me agrava la enfermedad?<br>
    El único que puede evaluar el riesgo/beneficio de su caso particular es su médico tratante quien podrá considerar un tratamiento profiláctico adecuado en aquellos pacientes que presenten riesgo de sufrir enfermedad ulcerosa gastrointestinal, sin embargo se recomienda que consulte con su médico.
    ",

    //4.29
    "29" => "<b>XARELTO</b><br>29. ¿Cuáles son las Interacciones que pueden presentarse con Xarelto®?<br>
    En particular, informe a su médico si toma:<br>
    <ul>
    <li>Ketoconazol</li>
    <li>Itraconazol</li>
    <li>Ritonavir</li>
    <li>lopinavir/ritonavir</li>
    <li>Indinavir</li>
    <li>Carbamazepina</li>
    <li>Fenitoína</li>
    <li>Fenobarbital</li>
    <li>Rifampina</li>
    <li>Hierba de San Juan (Hypericum perforatum)</li>    
    </ul>
    Pregunte a su médico si no está seguro si su medicamento es uno de los detallados arriba.
Sepa cuáles son los medicamentos que está tomando.<br>
Mantenga una lista para mostrársela al médico cuando reciba un nuevo medicamento<br>
(Ampliar información con la sección de interacciones de la CCDS del producto)
    ",

    //4.30
    "30" => "<b>XARELTO</b><br>30. ¿Qué contraindicaciones tiene Xarelto®?<br>
    <ul>
    <li>Xarelto® está contraindicado en los pacientes con hipersensibilidad a rivaroxabán o a cualquier excipiente del comprimido.</li>
    <li>Xarelto® está contraindicado en los pacientes con hemorragia activa, clínicamente significativa (p. ej., hemorragia intracraneal, hemorragia gastrointestinal).</li>
    <li>Xarelto® está contraindicado en los pacientes con enfermedad hepática la cual se asocia a coagulopatía llevando a un riesgo de hemorragia clínicamente relevante.</li>
    <li>No se ha establecido la seguridad y eficacia de Xarelto® en mujeres embarazadas. Los datos en animales demuestran que rivaroxabán atraviesa la barrera placentaria. Por lo tanto, el uso de Xarelto® está contraindicado durante el embarazo.</li>
    <li>No se ha establecido la seguridad y eficacia de Xarelto® en madres lactantes. Los datos en animales indican que rivaroxabán se secreta por la leche materna. Por lo tanto, Xarelto® solo debe administrarse después de interrumpir la lactancia materna.</li>
    </ul>
  <b>Si tiene dudas adicionales consulte a su médico tratante.</b>
    ",

    //4.31
    "31" => "<b>XARELTO</b><br>31. ¿Cuáles son los Eventos Adversos asociados con Xarelto®?<br>Las frecuencias de los EA reportados con Xarelto® se resumen en la tabla siguiente. Las reacciones adversas se presentan en orden decreciente de gravedad dentro de cada intervalo de frecuencia. Las frecuencias se definen como:<br>
    Muy frecuentes (≥ 1/10), <br>
    Frecuentes (≥ 1/100 a < 1/10),<br> 
    Poco frecuentes (≥ 1/1,000 a < 1/100), <br>
    Raras (≥ 1/10,000 a < 1/1,000)<br>
    ",//pendiente tabla

    //4.32
    "32" => "<b>XARELTO</b><br>32. ¿Puedo usar Xarelto® en estado de Embarazo o lactancia?<br>
    <ul>
    <li>Xarelto® deberá utilizarse en las mujeres en edad fértil sólo con medidas anticonceptivas efectivas.</li>
    <li>No se ha establecido la seguridad y eficacia de Xarelto en mujeres embarazadas.</li>
    <li>En ratas y conejos, rivaroxabán demostró una toxicidad materna marcada con cambios placentarios relacionados con su mecanismo de acción farmacológico (p. ej. complicaciones hemorrágicas) que ocasiona toxicidad en la reproducción. No se ha identificado ningún potencial teratógeno primario. Debido al riesgo intrínseco de hemorragia y a la evidencia de que rivaroxabán atraviesa la placenta, Xarelto® está contraindicado en el embarazo</li>
    <li>No se ha establecido la seguridad y eficacia de Xarelto en madres lactantes. En ratas, rivaroxabán se secreta por la leche materna. Por lo tanto, Xarelto® sólo debe administrarse después de interrumpir la lactancia materna.</li>
    </ul>
    ",

    //4.33
    "33" => "<b>XARELTO</b><br>33. Qué se debe hacer en caso de sobredosis?<br>
    Consulte a su médico tratante y/o al servicio de urgencia de su entidad de salud.
Una sobredosis de Xarelto® puede causar hemorragia. Interrumpa Xarelto® e inicie una terapia apropiada si ocurren complicaciones de hemorragia asociadas con una sobredosis.
Se han notificado casos raros de sobredosis de hasta 600 mg sin complicaciones hemorrágicas ni otros eventos adversos. Debido a la absorción limitada, es de esperar un efecto techo sin incremento adicional en la exposición plasmática promedio a dosis supraterapéuticas de 50 mg o superiores.<br>
No hay disponible ningún antídoto específico que antagonice el efecto farmacodinámico de rivaroxabán. Puede considerarse el uso de carbón activado para reducir la absorción en caso de sobredosis por Xarelto®. Debido a la elevada fijación a las proteínas plasmáticas, no se espera que rivaroxabán sea dializable.
    ",

    //4.34
    "34" => "<b>XARELTO</b><br>34. ¿Cuáles son las Precauciones especiales de conservación y manipulación?<br>
    En cuanto a los comprimidos, no se aplican restricciones en cuanto a la conservación (temperatura, humedad, luz).<br>
    Instrucciones de uso/manipulación: Ninguna<br>
Referencia:<br>
Documento Basado en CCDS Actual y revisión equipo médico<br>
Por favor si se presentan eventos adversos deben ser reportados a farmacovigilancia acorde con lo estipulado<br>
Las preguntas médicas no incluidas en este manual deben ser dirigidas a información médica
    ",

    //4.35
    "35" => "<b>XARELTO</b><br>35. ¿Qué medicamentos no puedo consumir si tomo Xarelto®?<br>
    Recomiende a los pacientes que informen a sus médicos y odontólogos si están tomando, o piensan tomar, cualquier medicamento recetado o de venta libre, o suplementos herbales, para que sus médicos puedan evaluar las posibles interacciones.<br>
Usted podría tener un mayor riesgo de hemorragia si toma Xarelto® y otros medicamentos que aumentan el riesgo de hemorragia, como los siguientes:<br>
<ul> <li>Aspirina</li> <li>AINES</li> <li>Antiplaquetarios</li> <li>Anticoagulantes orales o parenterales</li> </ul>
Informe a su médico si está tomando cualquiera de estos medicamentos. Pregunte al médico si no está seguro si su medicamento es uno de los detallados arriba.",

   
    //5.Información
    "5" => "<b>XARELTO<br>Información:</b><br>
    36. ¿Cuál es la lista de excipientes que se encuentran en una tableta de Xarelto®?<br>
    37. ¿Cómo se si Xarelto® está haciendo efecto? ¿Qué examen debo realizarme? ¿Por qué con Warfarina me tomaba el INR y ahora qué?<br>
    38. Tomo Xarelto® de 20 mg pero la EPS no me lo ha entregado y fui a comprarlo pero solo había de 15 mg, ¿la puedo tomar?<br>
    39. Tomo Xarelto® de 15 mg pero la EPS no me lo ha entregado y el fui a comprarlo pero solo había de 20 mg, ¿la puedo tomar?<br>
    40. ¿El uso de Hidróxido de aluminio para síntomas gástricos puede alterar la absorción de Xarelto®?<br>
    41. Por qué no lo cubre el Plan de beneficios ?<br>
    42. La EPS me ha demorado la entrega de Xarelto®, ¿puedo tomar Aspirina, o Warfarina mientras tanto para no quedar descubierto?
",
    
   //5.36
   "36" => "<b>XARELTO</b><br>36. ¿Cuál es la lista de excipientes que se encuentran en una tableta de Xarelto®?<br>
   <ul>
   <li>Celulosa microcristalina</li>
   <li>Croscarmelosa sódica</li>
   <li>Hipromelosa 5 cP</li>
   <li>Lactosa monohidratada</li>
   <li>Estearato de magnesio</li>
   <li>Laurilsulfato sódico</li>
   <li>Película de recubrimiento</li>
   <li>Óxido férrico rojo,</li>
   <li>Hipromelosa 15 cP</li>
   <li>Macrogol 3350,</li>
   <li>Dióxido de titanio</li>
   <li>Incompatibilidades</li>
   <li>Ninguna conocida</li>
   </ul>
   ",

   //5.37
   "37" => "<b>XARELTO</b><br>37. ¿Cómo se si Xarelto® está haciendo efecto? ¿Qué examen debo realizarme? ¿Por qué con Warfarina me tomaba el INR y ahora qué?<br>
   Los pacientes que estén tomando Xarelto® a la dosis fija recomendada es poco probable que esten sub o sobre-anticoagulados debido al perfil farmacológico predecible de rivaroxabán, el cual elimina la necesidad del monitoreo rutinario de la coagulación. Existen ensayos comerciales calibrados cuantitativos de anti-Factor Xa para aquellas circunstancias clínicas excepcionales en que la medición de los niveles plasmáticos de Xarelto® es útil.
   ",
   //5.38
   "38" => "<b>XARELTO</b><br>Tomo Xarelto® de 20 mg pero la EPS no me lo ha entregado y fui a comprarlo pero solo había de 15 mg, ¿la puedo tomar?<br>
   Consulte a su médico tratante.
   ",
   //5.39
   "39" => "<b>XARELTO</b><br> 39. Tomo Xarelto® de 15 mg pero la EPS no me lo ha entregado y el fui a comprarlo pero solo había de 20 mg, ¿la puedo tomar?<br>
   Consulte a su médico tratante.
   ",   
   //5.40
   "40" => "<b>XARELTO</b><br>40. ¿El uso de Hidróxido de aluminio para síntomas gástricos puede alterar la absorción de Xarelto®?<br>
   No se espera que el rivaroxabán presente interacciones clínicas relevantes con los medicamentos comúnmente usados para la protección gástrica como inhibidores de bomba de protones (por ej. Omeprazol), antagonistas del receptor H2 (por ej. ranitidina) o antiácidos.
   ",
   //5.41
   "41"=> "<b>XARELTO</b><br> 41. Por qué no lo cubre el Plan de beneficios ?<br>
   Xarelto® es un medicamento NO POS, sin embargo existen otras vías como los Mipres si su médico lo considera y lo justifica ante el pagador.",
   //5.42
   "42" => "<b>XARELTO</b><br>42. La EPS me ha demorado la entrega de Xarelto®, ¿puedo tomar Aspirina, o Warfarina mientras tanto para no quedar descubierto?<br>
      Consulte a su médico tratante.
   ",
   

  //MENÚ DE ADEMPAS/////////////////////////
   "adempas" => "Quieres saber de <b>ADEMPAS</b>. Elige un número<br>
   <b>43.</b> Formulacion<br>
   <b>44.</b> Descripción<br>
   <b>45.</b> Instrucciones<br>
   <b>46.</b> Precauciones<br>
   <b>47.</b> Información<br>
   <b>727.</b> Hablar con un Asesor",

   //43
   "43"=>"<b>ADEMPAS</b><br> 43. Formulación:<br>¿Cuál es la dosis óptima de Adempas®?<br>La dosis optima personalizada de Adempas® variará de acuerdo a las necesidades de cada paciente
   individual. En estudios clínicos, después de la titulación de ocho semanas, aproximadamente el 75% de
   los pacientes estaba recibiendo 2,5 mg tid, 15% 2 mg tid, 5% 1,5 mg tid, 3% 1 mg tid y 2% 0,5 mg tid de Adempas®. Adempas® permite una dosis por vía oral personalizada que es bien sustentada y manejada
   de manera sencilla.
   ",

   //44
   "44"=>"<b>ADEMPAS</b><br> 44. Descripción:<br>
  <b>j.</b> ¿Qué es Adempas® y para qué se usa?<br>
  <b>k.</b> ¿Cuál es la composición de Adempas®?<br>
  <b>l.</b> ¿Cuáles razones conllevaron al desarrollo de un nuevo medicamento como Adempas®?<br>
  <b>m.</b> ¿Cuál es el mecanismo de acción de Adempas®?<br>
  <b>n.</b> ¿Con qué rapidez se observa el beneficio de eficacia en los pacientes tratados con Adempas®?<br>
  <b>o.</b> ¿Cuáles son las presentaciones comerciales actualmente disponibles en Colombiade Adempas®?
   ",
    //44.J
   "j"=>"<b>ADEMPAS</b><br>J. ¿Qué es Adempas® y para qué se usa?<br> Adempas® es la marca bajo la cual comercializa el principio activo Riociguat. Riociguat es un
   estimulador de la guanilato-ciclasa soluble (GCs), que actúa dilatando las arterias pulmonares
   (los vasos sanguíneos que conectan el corazón con los pulmones), lo que le facilita al corazón
   bombear la sangre a través de los pulmones. ",

   //44.k
   "k"=>"<b>ADEMPAS</b><br>K. ¿Cuál es la composición de Adempas®?<br>Riociguat es el principio activo.",
   //44.l
   "l"=>"<b>ADEMPAS</b><br>L. ¿Cuáles razones conllevaron al desarrollo de un nuevo medicamento como Adempas®?<br>
   Antes de Adempas®, ningún tratamiento farmacológico oral había sido aprobado para, o demostrada eficacia en el tratamiento de HPTEC (excepto Ventavis® [iloprost inhalado]que en Colombia tiene este uso aprobado por INVIMA). Para la HAP, a pesar del desarrollo de tratamientos específicos, la mortalidad permanece alta en pacientes y ningún fármaco ha demostrado eficacia en pacientes con o sin tratamiento previo.<br>
   Adempas® es el primer tratamiento farmacológico oral aprobado para más de un tipo de HP y que muestra un rango tan amplio de eficacia. Abordando formas diferentes de HP, demuestra el compromiso de Bayer para comprender esta patología grave y mortal y ayudar a mejorar las vidas de las personas con HP.
   ",
   //44.m
   "m"=>"<b>ADEMPAS</b><br>M. ¿Cuál es el mecanismo de acción de Adempas®?<br>
   Riociguat está diseñado para restaurar la vía del ON-GCs-GMPc y disminuir la disfunción endotelial.
Adempas® se une y estimula directamente a la GCs, y también facilita la unión del óxido nítrico endógeno a la GCs. Por lo tanto, Adempas® funciona independientemente del óxido nítrico, lo que significa que también funciona en pacientes con niveles bajos de óxido nítrico endógeno. Esto puede explicar porque Adempas® tiene tanta eficacia en términos de más de un tipo de HP y múltiples criterios de valoración clínicos.",
   //44.n
   "n"=>"<b>ADEMPAS</b><br>N. ¿Con qué rapidez se observa el beneficio de eficacia en los pacientes tratados con Adempas®?<br>
   Para pacientes con HPTEC y HAP, Adempas® tiene un beneficio de eficacia temprano, dos semanas después del inicio del tratamiento, y este beneficio se volvió estadísticamente importante a las cuatro semanas.",
   //44.o
   "o"=>"<b>ADEMPAS</b><br>O. ¿Cuáles son las presentaciones comerciales actualmente disponibles en Colombiade Adempas®?<br><b>Comprimidos recubiertos:</b>
   <ul>
   <li>0.5 mg</li>
   <li>1.0 mg</li>
   <li>1.5 mg</li>
   <li>2.0 mg</li>
   <li>2.5 mg</li>
   </ul>
   ",
   //45
   "45"=>"<b>ADEMPAS</b><br>45. <b>Instrucciones:</b><br>
   <b>48.</b> ¿Cómo se debe tomar Adempas®?<br>
   <b>49.</b> ¿Cuál es la importancia de la prueba de la caminata de 6 minutos (6MWT)?<br>
   <b>50.</b> ¿Hay alguna recomendación para la toma de Adempas respecto a los alimentos y bebidas?<br>
   <b>51.</b> ¿Soy un paciente con insuficiencia hepática, qué recomendaciones debo tener para usar Adempas®?<br>
   <b>52.</b> ¿Soy un paciente con insuficiencia renal, que recomendaciones debo tener para usar Adempas®?
   ",
   //45.48
   "48"=>"<b>ADEMPAS</b><br>48. ¿Cómo se debe tomar Adempas®?<br>
   p. Adultos<br>
   q. Inicio del tratamiento<br>
   r. Dosis de mantenimiento<br>
   s. Suspensión del tratamiento
   ",

   //45.p
   "p"=>"<b>ADEMPAS</b><br>p. Adultos<br>Siga exactamente las instrucciones de administración de este medicamento indicadas por su médico. Si tiene duda, consulte a su médico.
   El tratamiento debe instaurarse y controlarse exclusivamente por un médico con experiencia en el tratamiento de la HPTEC o HAP.
   Durante las primeras semanas del tratamiento el médico medirá su presión arterial al menos cada dos semanas. Este control es necesario para decidir la dosis correcta del medicamento (Adempas® está disponible en diferentes concentraciones [0.5 mg a 2.5 mg]).",
  
   //45.q
   "q"=>"<b>ADEMPAS</b><br> q. Inicio del tratamiento<br>La dosis inicial recomendada de Adempas® en adultos es un comprimido de 1.0 mg tres veces al día durante 2 semanas.<br>
   Los comprimidos deben tomarse tres veces al día, con aproximadamente 6 a 8 horas de diferencia, con o sin alimentos.<br>
   Su médico aumentará la concentración de los comprimidos cada 2 semanas hasta un máximo de 2.5 mg tres veces al día (dosis diaria máxima de 7.5 mg), a no ser que presente algún efecto secundario o que tenga una presión arterial muy baja. Si presenta alguno de los efectos secundarios mencionados (véase más adelante el apartado 'Posibles efectos secundarios'), consulte a su médico.",

   //45.r
   "r"=>"<b>ADEMPAS</b><br>r. Dosis de mantenimiento<br>Su médico seguirá recetándole Adempas® a la dosis máxima que sea confortable para usted, a no ser que presente algún efecto secundario o que tenga la presión arterial muy baja. Si presenta alguno de los efectos secundarios mencionados (véase más adelante el apartado'Posibles efectos secundarios'), consulte a su médico.",

   //45.s
   "s"=>"<b>ADEMPAS</b><br>s. Suspensión del tratamiento<br>Consulte a su médico antes de reiniciar el tratamiento en caso de que tenga que interrumpir el tratamiento durante 3 días o más.",

   //45.49
   "49"=>"<b>ADEMPAS</b><br>49. ¿Cuál es la importancia de la prueba de la caminata de 6 minutos (6MWT)?<br>La prueba de la caminata de 6 minutos es el criterio de valoración estándar utilizado en estudios clínicos para pacientes con HP.<br>Su uso también está respaldado por las guías actuales. Es una medida de la capacidad de ejercicio de los pacientes y es un predictor de supervivencia en pacientes con HP, y por lo tanto tiene un significado tangible para los pacientes.",

   //45.50
   "50"=>"<b>ADEMPAS</b><br>50. ¿Hay alguna recomendación para la toma de Adempas respecto a los alimentos y bebidas?<br>Adempas® puede tomarse con y sin alimentos.",

   //45.51
   "51"=>"<b>ADEMPAS</b><br>51. ¿Soy un paciente con insuficiencia hepática, qué recomendaciones debo tener para usar Adempas®?<br>
   No hubo cambios clínicamente relevantes en la exposición en sujetos cirróticos con insuficiencia hepática leve (clasificado como grado A de Child-Pugh).
En sujetos cirróticos con insuficiencia hepática moderada (clasificado como grado B de Child Pugh), la media del ABC de riociguat aumentó en 50 -70% en comparación con los controles sanos (véase el apartado 'Pauta posológica' CCDS).
No existen datos en pacientes con insuficiencia hepática severa (clasificado como grado C de Child-Pugh); por consiguiente, no se recomienda la administración de Adempas® a estos pacientes (véase el apartado 'Pauta posológica' CCDS, 'Advertencias y precauciones especiales de empleo').",

    //45.52
    "52"=>"<b>ADEMPAS</b><br>52. ¿Soy un paciente con insuficiencia renal, que recomendaciones debo tener para usar Adempas®?<br>
    En general, la media de la exposición al riociguat, normalizada para tener en cuenta la dosis y el peso, fue mayor en sujetos con insuficiencia renal que en sujetos con función renal normal. Los valores correspondientes para el metabolito principal fueron mayores en sujetos con insuficiencia renal que en sujetos sanos. En los sujetos con insuficiencia renal leve (aclaramiento de creatinina de 80 -50 mL/min), moderada (aclaramiento de creatinina < 50 -30 mL/min) o severa (aclaramiento de creatinina < 30 mL/min) las concentraciones plasmáticas de riociguat (ABC) aumentaron en un 43%, 104% y 44% respectivamente (véase el apartado 'Pauta posológica').<br>
No existen datos en pacientes con aclaramiento de creatinina < 15 mL/min o sometidos a diálisis. Por consiguiente, no se recomienda la administración a pacientes con aclaramiento de creatinina < 15 mL/min o sometidos a diálisis (véanse los apartados 'Pauta posológica' y 'Advertencias y precauciones especiales de empleo' en la CCDS).<br>
Debido a la elevada unión del riociguat a las proteínas plasmáticas, no se espera que sea dializable.
    ",

    //46
    "46"=>"<b>ADEMPAS</b>Elije un número<br>46. Precauciones:<br>
    <b>53.</b> ¿Qué necesito saber antes de tomar Adempas®?<br>
    <b>54.</b> ¿Cuáles son las advertencias y precauciones que debo tener si tomo Adempas®?<br>
    <b>55.</b> ¿A mi hijo que es menor de edad le diagnosticaron HP, puede tomar Adempas ®?<br>
    <b>56.</b> ¿Qué se sabe sobre la seguridad y tolerabilidad de riociguat?<br>
    <b>57.</b> ¿Qué debe hacer un paciente si se toma una dosis más de Adempas®?<br>
    <b>58.</b> ¿Qué debe hacer un paciente si olvida tomar una dosis de Adempas®?<br>
    <b>59.</b> ¿Qué pasa si dejo de tomar Adempas®?<br>
    <b>60.</b> ¿Qué efectos secundarios relacionados al tratamiento están asociados con Adempas®?<br>
    <b>61.</b> ¿Se puede administrar Adempas® en combinación con otros tratamientos específicos de HP?<br>
    <b>62.</b> ¿Soy fumador, puedo tomar Adempas®?<br>
    <b>63.</b> ¿Puedo tomar Adempas® durante el embarazo y/o la lactancia?<br>
    <b>64.</b> ¿Puedo conducir durante el tratamiento con Adempas®?<br>
    <b>65.</b> ¿Qué precauciones debo tener para la conservación de Adempas®?
",

   //46.53
    "53"=>"<b>ADEMPAS</b><br>53. ¿Qué necesito saber antes de tomar Adempas®?<br>
    No tome Adempas®<br>
    <ul>
    <li>sí está embarazada.</li>
    <li>sí está tomando nitratos (medicamentos utilizados para el tratamiento de la presión arterial alta o enfermedades cardiacas) o donadores de óxido nítrico (tales como nitrito de amilo) de cualquier forma.</li>
    <li>sí está tomando inhibidores de la PDE-5 (tales como sildenafil o tadalafil) utilizados para el tratamiento de la presión elevada en las arterias pulmonares (hipertensión arterial pulmonar) o de la disfunción eréctil (como los mencionados anteriormente o vardenafil).</li>
</ul>
Frente a cualquier otra inquietud relacionada con su tratamiento, por favor comuníquelo a su médico tratante.
    ",

   //46.54
   "54"=>"<b>ADEMPAS</b><br>54. ¿Cuáles son las advertencias y precauciones que debo tener si tomo Adempas®?<br>
   Consulte a su médico antes de tomar Adempas®<br>
• Sí siente dificultad para respirar durante el tratamiento con Adempas®, puede deberse a una acumulación de líquido en los pulmones (enfermedad venooclusiva pulmonar). Consulte a su médico.<br>
• Sí ha tenido recientemente sangrado pulmonar grave o si ha sido sometido a una intervención quirúrgica para detener la expectoración de sangre (embolización de arterias bronquiales). En este caso puede aumentar el riesgo de sangrado pulmonar. Informe a su médico si está tomando medicamentos para prevenir la formación de coágulos sanguíneos (anticoagulantes). El médico controlará de forma periódica su situación.<br>
• Sí tiene problemas del corazón o circulatorios o si está en tratamiento antihipertensivo.<br>
• Sí está tomando medicamentos para el tratamiento de infecciones por hongos (p. ej., ketoconazol, itraconazol) o medicamentos para el tratamiento de la infección por VIH (p. ej., ritonavir).<br>
• Sí está tomando medicamentos contra el cáncer denominados inhibidores de la tirosina-cinasa (p. ej., erlotinib, gefitinib) o ciclosporina: un medicamento para prevenir el rechazo de órganos trasplantados. En este caso su médico tendrá que controlar la presión arterial de forma periódica.<br>
• Sí tiene presión arterial baja.<br>
• Sí tiene problemas del hígado, si tiene problemas de los riñones o si está sometido a diálisis.
   ",

   //46.55
   "55"=>"<b>ADEMPAS</b>",
   
   //46.56
   "56"=>"<b>ADEMPAS</b>",
   
   //46.57
   "57"=>"<b>ADEMPAS</b>",
   
   //46.58
   "58"=>"<b>ADEMPAS</b>",
   
   //46.59
   "59"=>"<b>ADEMPAS</b>",

   //46.60
   "60"=>"<b>ADEMPAS</b>",
   
   //46.61
   "61"=>"<b>ADEMPAS</b>",
   
   //46.62
   "62"=>"<b>ADEMPAS</b>",

   //46.63
   "63"=>"<b>ADEMPAS</b>",

   //46.64
   "64"=>"<b>ADEMPAS</b>",

   //46.65
   "65"=>"<b>ADEMPAS</b>",

    //MENÚ DE VENTAVIS/////////////////////////

    "ventavis"=>"Quieres saber de <b>VENTAVIS</b>.  Elige un número<br>
    <b>49.</b> Formulacion<br>
    <b>50.</b> Descripción<br>
    <b>48.</b> Instrucciones<br>
    <b>51.</b> Precauciones<br>
    <b>52.</b> Información<br>
    <b>727.</b> Hablar con un Asesor",

    //MENÚ DE NEXAVAR/////////////////////////

    "nexavar"=>"Quieres saber de <b>NEXAVAR</b>.  Elige un número<br>
    <b>53.</b> Formulacion<br>
    <b>54.</b> Descripción<br>
    <b>55.</b> Instrucciones<br>
    <b>56.</b> Precauciones<br>
    <b>57.</b> Información<br>
    <b>727.</b> Hablar con un Asesor",

    //MENÚ DE EYLIA/////////////////////////

    "eylia"=>"Quieres saber de <b>EYLIA</b>.  Elige un número<br>
    <b>58.</b> Formulacion<br>
    <b>59.</b> Descripción<br>
    <b>60.</b> Instrucciones<br>
    <b>61.</b> Precauciones<br>
    <b>62.</b> Información<br>
    <b>727.</b> Hablar con un Asesor",




    //KOGENATE

    "kogenate" => "Quieres saber de kogenate elige un numero<br>
    80. Formulacion<br>
    81. Descripción<br>
    82. Instrucciones<br>
    83. Precauciones<br>
    84. Información",



    //Causales por las cuales puede pasar a un asesor
    "dolor" => "Usted tiene un <b>dolor</b> espere un momento lo estamos remitiendo a un asesor...",
    "enfermo" => "Usted se encuentra <b>enfermo</b> espere un momento lo estamos remitiendo a un asesor...",
    "enferma" => "Usted se encuentra <b>enferma</b> espere un momento la estamos remitiendo a un asesor...",
    "morir" => "ups! ya lo estamos contactando con un experto...",
    "duele" => "Usted tiene un <b>dolor</b> espere un momento lo estamos remitiendo a un asesor...",
    "asesor" => "Usted tiene una dificultad lo estamos <b>contactando con un asesor...</b>",

    //numero de atención con asesor
    "727" => "Has marcado el numero de atención<br>te estamos <b>contactando con un asesor</b>...",


    //name
    "como te llamas?" => "Soy Educadora y estoy para servirte",
    "como te llamas" => "Soy Educadora y estoy para servirte",
    "cual es tu nombre?" => "Soy Educadora y estoy para servirte",
    "cual es tu nombre" => "Soy Educadora y estoy para servirte",
    "tienes nombre?" => "Soy Educadora y estoy para servirte",
    "tienes nombre" => "Soy Educadora y estoy para servirte",
    "como me llamo yo" => "por tu nombre",
    "como me llamo" => "por tu nombre",
    "cuantos anos tienes" => "mi edad depende de mis creadores",



    //Chistes
    "cuentame un chiste" => "Me se uno<br>-Por favor, ayúdeme, mi hija se ha perdido <br> <b>¿como se llama?</b><br>Esperanza<br><b>Imposible, la esperanza es lo último que se pierde.</b><br>JaJaJa",
    "sabes un chiste?" => "Me se uno<br>-Por favor, ayúdeme, mi hija se ha perdido <br> <b>¿como se llama?</b><br>Esperanza<br><b>Imposible, la esperanza es lo último que se pierde.</b><br>JaJaJa",
    "sabes un chiste" => "Me se uno<br>-Por favor, ayúdeme, mi hija se ha perdido <br> <b>¿como se llama?</b><br>Esperanza<br><b>Imposible, la esperanza es lo último que se pierde.</b><br>JaJaJa",
    "chiste" => "Me se uno<br>-Por favor, ayúdeme, mi hija se ha perdido <br> <b>¿como se llama?</b><br>Esperanza<br><b>Imposible, la esperanza es lo último que se pierde.</b><br>JaJaJa",
    "sabes algún chiste?" => "Me se uno<br>-Por favor, ayúdeme, mi hija se ha perdido <br> <b>¿como se llama?</b><br>Esperanza<br><b>Imposible, la esperanza es lo último que se pierde.</b><br>JaJaJa",
    "te sabes algún chiste?" => "Me se uno<br>-Por favor, ayúdeme, mi hija se ha perdido <br> <b>¿como se llama?</b><br>Esperanza<br><b>Imposible, la esperanza es lo último que se pierde.</b><br>JaJaJa",
    "otro chiste" => "¡Cómo se llama el pato que no se lleva bien con otros patos?<br> ANTIPATICO<br>JaJaJa",
    //despedida
    "adios" => "cuidate",
    "hasta la proxima" => "nos vemos pronto",
    "nos vemos" => "te estare esperando",
    "bye" => "Good bye ♥",
    "see you" => "see you lader ♥",
    "chao" => "Chao",
    "hasta luego" => "Hasta luego",
    "muchas gracias" => "con gusto",
    "gracias" => "con gusto",
    "usted no sabe nada" => "Estoy en costrucción, pero intento mejorar",
    "gracias por tu ayuda" => "con gusto",
    "que mal servicio"=>"Intento hacer lo mejor<br>Deseas comunicarte con un <b>asesor digita 727<b>",
    "pesimo servicio"=>"Intento hacer lo mejor<br>Deseas comunicarte con un <b>asesor digita 727<b>",

    //
    "what is your name?" => " my name is Educadora",

    "farmacovigilancia"=>"<a href='#'>da click aqí</a>",

    "tu nombre es?" => "Mi nombre es " . $bot->getName(),
    "tu eres?" => "Yo soy una " . $bot->getGender()

];

function quitar_tildes($comprobar)
{
    $no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ", "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹");
    $permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
    $texto = str_replace($no_permitidas, $permitidas, $comprobar);
    return $texto;
}

if (isset($_GET['msg'])) {


    $palabras = quitar_tildes($_GET['msg']);
    $palabras_ =  strtolower($palabras);
    $msg = preg_replace('/[.]/', '', $palabras_);

    $bot->hears($msg, function (Bot $botty) {
        global $msg;
        global $questions;

        if (strtolower($msg) == 'hi' || strtolower($msg) == "hello") {
            $botty->reply('Hola en que puedo ayudarte');
        } elseif ($botty->ask(strtolower($msg), $questions) == "") {
            if (preg_match("/dolor/i", $msg)) {
                $msg = 'dolor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/enfermo/i", $msg)) {
                $msg = 'enfermo';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/enferma/i", $msg)) {
                $msg = 'enferma';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/morir/i", $msg)) {
                $msg = 'morir';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/duele/i", $msg)) {
                $msg = 'duele';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/abdominal/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/aborto/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/acidez/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/adinamia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/afasia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/agrieras/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/alergia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/amnesia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/anemia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/ansiedad/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/ardor/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/arritmia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/artralgias/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/astenia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/ataque/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/ataxia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/atrofia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/baja de tension/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/boca seca/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/brote/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/caida de cabello/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/calor local/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/calvicie/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/cansancio/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/cefalea/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/ceguera/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/confusion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/congestion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/convulsion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/debilidad/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/depresion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/deshidratacion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/desmayo/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/diarrea/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/dificultad para/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/disartria/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/disfagia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/disfuncion erectil/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/distension/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/dolor/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/dolor articular/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/dormido/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/enrojecimiento/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/erupcion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/escalofrio/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/estrenimiento/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/fatiga/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/fiebre/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/flatulencia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/foto sensibilidad/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/fotofobia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/fractura/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/gases/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/gastritis/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/gastroenteritis/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/gripe/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/hinchazon/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/hipotension/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/hormigueo/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/inapetencia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/incoherencias/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/incontinencia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/infarto/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/infeccion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/inflamacion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/insomnio/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/insuficiencia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/intolerancia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/irritacion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/letargia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/llanto facil/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/llenura/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/lumbalgia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/malestar/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/mareos/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/moretones/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/nauseas/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/no puedo tragar/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/palidez/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/palpitaciones/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/paralisis/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/perdida de apetito/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/pesadez/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/picazon/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/presion alta/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/rash cutaneo/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/reflujo/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/respirar/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            }elseif (preg_match('/respiracion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } 
            elseif (preg_match('/ruptura vasos/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/sangrado/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/sangrando/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/ayuda/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/sangre/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/sanguineos/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/somnolencia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/sudoracion/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/taquicardia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/tos/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/traumatismo/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/trombocitopenia/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/ulcera/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/urticaria/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/vision borrosa/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/vomito/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/vomitos/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/nauseas/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match('/mareo/i', $msg)) {
                $msg = 'asesor';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/kogenate/i", $msg)) {
                $msg = 'kogenate';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/adempas/i", $msg)) {
                $msg = 'adempas';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/ventavis/i", $msg)) {
                $msg = 'ventavis';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/betaferon/i", $msg)) {
                $msg = 'betaferon';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/eylia/i", $msg)) {
                $msg = 'eylia';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/nexavar/i", $msg)) {
                $msg = 'nexavar';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/stivarga/i", $msg)) {
                $msg = 'stivarga';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/xofigo/i", $msg)) {
                $msg = 'xofigo';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/xarelto/i", $msg)) {
                $msg = 'xarelto';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/hola/i", $msg)) {
                $msg = 'hola';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            }elseif (preg_match("/buenas tardes/i", $msg)) {
                $msg = 'buenas tardes';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            }elseif (preg_match("/buenas dias/i", $msg)) {
                $msg = 'buenas dias';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } elseif (preg_match("/buenas noches/i", $msg)) {
                $msg = 'buenas noches';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            }  elseif (preg_match("/buen dia/i", $msg)) {
                $msg = 'buen dia';
                $botty->reply($botty->ask(strtolower($msg), $questions));
            } 

            else {
                $botty->reply("No entiendo tu pregunta tal vez estas opciones puedan ayudar:<br>
                Quieres inscribirte al programa de Pacientes? ingresa 727<br>
                Quieres saber de algun medicamento? ingresa el nombre del medicamento <span class='text-success' >ejemplo: <b>adempas<b></span>
                ");
            }
        } else {

            $botty->reply($botty->ask(strtolower($msg), $questions));
        }
    });
}
