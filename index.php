<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bayer </title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>

<body>
    <div class="row col-md-12">
        <div class="col-md-5">
            <img src="img/doctora_fon.png" class="w-100" alt="">
        </div>
        <div class="col-md-6">
        <div class="container">


            <div class="chatbox row col-md-10 mx-auto" style="height: 350px">
                <div class="header">
                    <h4> <img src='img/ima_perfil.png' class='imgRedonda' /> Ana a tu servicio </h4>

                </div>

                <div class="body col-md-12" id="chatbody" style="overflow: auto;">
                    <p class="alicia"><?php //Ejemplo curso PHP aprenderaprogramar.com Australia/Tasmania
    date_default_timezone_set('America/Bogota');
    $hora =date("H");
    if($hora > 12 && $hora < 18){
    echo "Buenas Tardes";
    }elseif($hora > 18 && $hora < 23){
        echo "Buenas Noches";
    }
    elseif($hora >= 0 && $hora < 4){
        echo "Buen día";
    }
    elseif($hora > 4 && $hora < 12){
        echo "Buenos Días";
    }


    ?> mi nombre es Ana, soy del Programa de Atención a Pacientes de Bayer Contigo, ¿en qué puedo ayudarte?</p>
                    <div class="scroller"></div>
                </div>

                <form class="chat body col-md-12" method="post" autocomplete="off">

                    <div class="row col-md-12">
                        <center> <input type="text" name="chat" id="chat" placeholder="Preguntale algo"
                                class="form-control mt-5" style="width:100%;"></center>
                    </div>
                    <div class="row col-md-12">
                        <center>
                            <button type="submit" class="btn btn-dark" style="width:100%" id="btn">Enviar</button>
                        </center>
                    </div>
                </form>

                <input type="hidden" class="creador" value="Creadores" onClick="mi_alerta()">
            </div>
        </div>

        <script src="app.js"></script>

        <SCRIPT LANGUAGE="JavaScript">
        function mi_alerta() {
            alert("Tutoriales" +
                "\n" +
                "\nCaleb & Mr. Luna");
        }
        </SCRIPT>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
        </script>
        </div>
    </div>
</body>

</html>